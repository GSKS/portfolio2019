import React, { Component } from 'react';
import WebFont from 'webfontloader';
import '../style/Home.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'


export default class Home extends Component {
  render() {
    WebFont.load({
        google: {
          families: ['Ubuntu:700', 'sans-serif']
        }
      });

    let resumeData = this.props.data;
      const id={
          mainDiv       :'detailsContainer',
          name          :'name',
          expertise     :'expertise',
          skills        :'skills',
          skill         :'skill',
          button        :'know-me'
      }

    const skills = resumeData.expertise.map((skill)=>
        <li id={id.skill}>{skill}</li>
    );
    return (
    <Container >
        <Row className="text-center homeContainer">
            <div id={id.mainDiv}>
                <div id={id.name}>
                        {resumeData.name}
                </div>
                <div id={id.expertise}>
                        <ul id={id.skills}>
                            {skills}
                        </ul>
                </div>
                
                <div className={id.button}>
                  <button>Click to know more about me!</button>
                </div>


            </div>
      </Row>
      </Container>
    )
  }
}
     
