import React, { Component } from 'react';
import {Navbar,Nav} from 'react-bootstrap';


export default class Header extends Component {
  render() {
    return (
        <Navbar bg="transparent" expand="lg">
        <Navbar.Brand href="#home">Senthil Kumaran Santhosh</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="home" className="linkText">Home</Nav.Link>
           
            
          </Nav>
            <Nav>
            <Nav.Link href="#education" className="linkText">Education</Nav.Link>
            <Nav.Link href="#work" className="linkText">Work</Nav.Link>
            <Nav.Link href="#projects" className="linkText">Projects</Nav.Link>
            <Nav.Link href="me" className="linkText">About Me</Nav.Link>
            </Nav>
         
           
 
        </Navbar.Collapse>
      </Navbar>
    )
  }
}
