import React, { Component } from 'react';
import Header from './header.js';

export default class Root extends Component {
  render() {
    return (
      <div className=".container-fluid">
        <div  className="row justify-content-md-center">
            <div className="col">

                <Header/>
           </div>
        </div>
        
        <div className="row justify-content-md-center">
            <div className="col-6">
                {this.props.children}
            </div>

        </div>
      </div>
    )
  }
}
