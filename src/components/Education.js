import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

export default class Education extends Component {
  
  render() {
    const gridStyle={
        margin:'10px'
    }
    const styles={
        color:'red'
    }  
    const flxStyle={
        display:'flex',

    
        

    }
    return (
      <div>
          <Grid item xs={12} style={gridStyle}>
              <Paper style={{backgroundColor:'#00000000'}}>
                  <section className="container">
                    <p style={styles}>education</p>
                    <div style={flxStyle}>
                        <div>
                            BE.ECE
                        </div>
                        <div>
                            Master of IT
                        </div>

                    </div>
                  </section>
              </Paper>
          </Grid>
      </div>
    )
  }
}
