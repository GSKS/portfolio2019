import React, { Component } from 'react';

import PropTypes from 'prop-types';
import '../style/AboutMe.css';



export default class AboutMe extends Component {
    constructor(props) {
        super(props);
       
        
        var today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        



        const styles = theme => ({
          root: {
            flexGrow: 1,
          },
          paper: {
            padding: theme.spacing.unit * 2,
            textAlign: 'center',
            color: "#EBF0DC",
            
          },

        });
       this.state = {
            date: date,
            style:styles
        };
      }
    
    
    
  render() {
    let resumeData = this.props.data;
    let id="aboutHeading";
    
    const data = {
        name:"Senthil Kumaran Santhosh Gopal",
        current:"Information technology student",
        uniWeb:"https://www.auckland.ac.nz/en.html"

    };
    return (
     
          <section  className="about-container">
          
                <div className="about">
                        <h1 id="about">about.me</h1>
                        <h2 id="current">
                           {data.current}
                        </h2>
                        <div>
                        <p id="details">
                          I am an ICT professional who is currently studying Master of Information Technology 
                          in the <a id="uniWeb" href={data.uniWeb}>University of Auckland</a>
                          <br></br>
                          <br></br>
                          
                          I am also an experienced .net and Java developer.I can built websites using JS,HTML,css
                          and this site being built using REACTjs,I am also familiar with REACTJS framework.
                          <br></br>
                          <br></br>
                          I love to explore new tech things and learn to implement and try to develop some small side projects in my spare time.
                          
                          

                        </p>
                        </div>
                        <div className="borderBot">

                        </div>

                        <div className="meta-info">
                            <p className="date" >{this.state.date}</p>
                            <p className="author">By:{data.name}</p>
                            

                        </div>

                    
                </div>


         
                </section>
      
    )
  }
}
