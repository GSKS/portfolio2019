import React from 'react';

import AboutMe from './components/AboutMe';
import Education from './components/Education';
import Home from './components/Home';
import Root from './components/Root'
import './App.css';
import data from './data.js'
import {Switch,Route,BrowserRouter} from 'react-router-dom';




function App() {
  
  return (
    // <div>
    
    <div>
     
      <Root>

        <Switch>
          <BrowserRouter>  
            <Route path="/home" render={(props) => (
                <Home {...props} data={data}/>
            )}/>
            <Route path="/me" component={AboutMe}/>
          </BrowserRouter>
        </Switch>


      </Root>
    </div>  

      

    //   {/* <Header data={data}/> 
    //   <Home data={data}/>
    //   {/* <AboutMe data={data} style={{display:'none'}}/>
    //   <Education/> */} 
    // 
    
    );
}

export default App;
